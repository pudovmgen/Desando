package com.android.desando.app;

import com.android.desando.app.api.DesandoApiService;
import com.android.desando.app.ui.restoran.presenter.RestoranPresenter;
import com.android.desando.app.utilities.app.ApplicationApp;

import org.junit.Test;

import java.security.cert.CertificateException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.SimpleTimeZone;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import dagger.Provides;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
    }


    @Test
    public void cuttentTime() {
        RestoranPresenter obj = new RestoranPresenter(null, null);
        System.out.print(obj.getCurrentDate());
    }

    @Test
    public void testList(){
        for (int i = 0; i< 50; i++){
            System.out.println(i % 16);
        }
    }

    @Test
    public void parsTime() {
        String time = "12:00 - 01:00";

        String startTime = time.split("-")[0].trim();
        String endTime = time.split("-")[1].trim();
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        Date dateOne = null;
        Date dateTwo = null;

        try {
            dateOne = format.parse(startTime);
            dateTwo = format.parse(endTime);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        long difference = dateOne.getTime() - dateTwo.getTime();
        int days =  (int)(difference / (60 * 60 * 1000));
        System.out.print("{\n"+
                        difference+ ",\n"+
                        days+
                "\n}");

/*        Integer startHour = Integer.valueOf(startTime.split(":")[0]);
        Integer startMimut = Integer.valueOf(startTime.split(":")[1]);

        Integer endHour = Integer.valueOf(endTime.split(":")[0]);
        Integer endMimut = Integer.valueOf(endTime.split(":")[1]);*/


/*        System.out.print("{\n"+
                "startTime: " + startTime +",\n"+
                "endTime: " + endTime +",\n"+
                "startHour: " + startHour +",\n"+
                "startMimut: " + startMimut +",\n"+
                "endHour: " + endHour +",\n"+
                "endMimut: " + endMimut +",\n"+
                "}"
        );*/
    }

    @Test
    public void downloadFileRetrofit(){
        DesandoApiService service = provideDesandoApiService();
        service.downloadPdf("http://kmmc.in/wp-content/uploads/2014/01/lesson2.pdf")
                .subscribe(
                        body -> {
                            System.out.print(body.bytes().length);
                        },
                        throwable -> {
                            System.out.print("Ошибка");
                        }
                );

    }



    private Retrofit getRetrofit(){
        OkHttpClient client =  provideOkHttpClient(
                provideSSLContext(provideTrustManager()),
                provideTrustManager()
        );

        return new Retrofit.Builder()
                .baseUrl("https://desando.ru/")
                .client(client)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

    }

    public OkHttpClient provideOkHttpClient(SSLSocketFactory sslSocketFactory, TrustManager[] trustManager){
        return new OkHttpClient.Builder()
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .sslSocketFactory(sslSocketFactory,
                        (X509TrustManager)trustManager[0])
                .hostnameVerifier((hostname, session) -> true)
                .build();
    }

    public SSLSocketFactory provideSSLContext(TrustManager[] trustManager) {
        final SSLContext sslContext;
        try {
            sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustManager, new java.security.SecureRandom());
            return sslContext.getSocketFactory();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public TrustManager[] provideTrustManager(){
        return new TrustManager[] {
                new X509TrustManager() {
                    @Override
                    public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                    }

                    @Override
                    public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                    }

                    @Override
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return new java.security.cert.X509Certificate[]{};
                    }
                }
        };
    }

    public DesandoApiService provideDesandoApiService(){
        return getRetrofit().create(DesandoApiService.class);
    }


}