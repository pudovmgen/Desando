package com.android.desando.app.utilities.adapter;

public interface ItemType {
    int getType();
}
