package com.android.desando.app.ui.restoran.model;

import android.annotation.SuppressLint;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class RestoranDateTime implements Serializable {
    Calendar calendar;
    public RestoranDateTime(Calendar calendar) {
        this.calendar = calendar;
    }

    public void setDate(int dayOfMonth, int month, int year){
        calendar.set(year, month, dayOfMonth, getHourOfDay(), getMinute());
    }

    public void setTime(int hourOfDay, int minute){
        calendar.set(getYear(), getMonth()-1, getDayOfMonth(), hourOfDay, minute);
    }

    public void addDays(int countDay){
        calendar.add(Calendar.DATE, countDay);
    }

    public void removeDays(int countDay){
        calendar.add(Calendar.DATE, -countDay);
    }

    public void removeMonth(int countMonth){
        calendar.add(Calendar.MONTH, -countMonth);
    }

    public void addMonth(int countMonth){
        calendar.add(Calendar.MONTH, countMonth);
    }

    @SuppressLint("SimpleDateFormat")
    private int getDateTimeFormat(String s){
        return Integer.parseInt(
                new SimpleDateFormat(s)
                                .format(getDate())
        );
    }

    @Override
    public String toString() {
        return new SimpleDateFormat("ddMMyyyyHHmm")
                    .format(getDate());
    }

    public int getYear() {
        return getDateTimeFormat("yyyy");
    }

    public int getMonth() {
        return getDateTimeFormat("MM");
    }

    public int getDayOfMonth() {
        return getDateTimeFormat("dd");
    }

    public int getHourOfDay() {
        return getDateTimeFormat("HH");
    }

    public int getMinute() {
        return getDateTimeFormat("mm");
    }

    public Date getDate(){
        return calendar.getTime();
    }

    public String getCurrentDate(){
        return new SimpleDateFormat("dd MMMM yyyy").format(getDate());
    }

    public String getCurrentTime(){
        return new SimpleDateFormat("HH-mm").format(getDate());
    }
}
