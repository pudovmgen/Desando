package com.android.desando.app.ui.history.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class HistoryBean implements Serializable
{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("restoran")
    @Expose
    private String restoran;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("image")
    @Expose
    private String image;
    private final static long serialVersionUID = -2287429144507332544L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            return new SimpleDateFormat("dd MMMM yyyy в HH:mm").format(dateFormat.parse(date));
        } catch (ParseException e) {return "";}
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getOrderDate(){
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            return new SimpleDateFormat("dd MMMM yyyy").format(dateFormat.parse(date));
        } catch (ParseException e) {return "";}
    }

    public String getOrderTime(){
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            return new SimpleDateFormat("HH:mm").format(dateFormat.parse(date));
        } catch (ParseException e) {return "";}
    }

    public String getRestoran() {
        return restoran;
    }

    public void setRestoran(String restoran) {
        this.restoran = restoran;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

}