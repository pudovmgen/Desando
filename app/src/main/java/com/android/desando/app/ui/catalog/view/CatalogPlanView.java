package com.android.desando.app.ui.catalog.view;

import com.android.desando.app.ui.catalog.model.FeaturesBean;

//Сценарий
public interface CatalogPlanView {
    //Первый вход
    void primaryEntrance();
    //выбор тега
    void selectionFeatures(FeaturesBean features);
    //поиск
    void selectionSearch();
    //
    //void comeBack();
}
