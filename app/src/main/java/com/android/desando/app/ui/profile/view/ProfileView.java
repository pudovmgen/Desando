package com.android.desando.app.ui.profile.view;

import com.android.desando.app.utilities.mvp.MvpView;

import java.util.List;

public interface ProfileView extends MvpView {
    void setAdapterSpinner(List<String> items, String idDefault,String nameDefault);

    void hideList();

    void showList();

    void showLoad();

    void hideLoad();
}
