package com.android.desando.app.ui.history.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class HistoryInfoResponse implements Serializable
{

    @SerializedName("pageInfo")
    @Expose
    private HistoryInfo pageInfo;
    @SerializedName("elements")
    @Expose
    private List<HistoryBean> elements = null;
    private final static long serialVersionUID = -7612097070547753231L;

    public HistoryInfo getPageInfo() {
        return pageInfo;
    }

    public void setPageInfo(HistoryInfo pageInfo) {
        this.pageInfo = pageInfo;
    }

    public List<HistoryBean> getElements() {
        return elements;
    }

    public void setElements(List<HistoryBean> elements) {
        this.elements = elements;
    }

}
