package com.android.desando.app.ui.order.activity;

import com.android.desando.app.ui.order.view.OrderView;
import com.android.desando.app.ui.review_restoran.model.OrderTableAbout;
import com.android.desando.app.utilities.activity.DesandoActivity;

public class OrderActivity extends DesandoActivity implements OrderView {
    @Override
    public void showLayout() {

    }

    @Override
    public void hideLayout() {

    }

    @Override
    public void setAbout(OrderTableAbout about) {

    }

    @Override
    public void openImage() {

    }

    @Override
    public void chooseDate() {

    }

    @Override
    public void chooseTime(String time) {

    }

    @Override
    public void checkUserAuth() {

    }

    @Override
    public void messageOrder() {

    }

    @Override
    public void messageSelectTimeOrder() {

    }

    @Override
    public void tableOrder() {

    }

    @Override
    public void successOrder() {

    }

    @Override
    public void ErrorOrder(String s) {

    }

    @Override
    public void openDialogMens() {

    }

    @Override
    protected void initToolbar() {

    }

    @Override
    protected int initContentView() {
        return 0;
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initDaggerInject() {

    }
}
