package com.android.desando.app.ui.review_restoran.presenter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;

import com.android.desando.app.api.DesandoApiService;
import com.android.desando.app.ui.restoran.model.RestoranDateTime;
import com.android.desando.app.ui.review_restoran.view.ReviewRestoranView;
import com.android.desando.app.utilities.app.User;
import com.android.desando.app.utilities.mvp.BasePresenter;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class OrderPresenter extends BasePresenter<ReviewRestoranView>  {
    DesandoApiService service;
    public RestoranDateTime dateTime;
    public String restoranId;
    public String tabId;
    User user;

    public OrderPresenter(DesandoApiService service, String restoranId, String tabId, RestoranDateTime dateTime, Context context) {
        this.service = service;
        this.restoranId = restoranId;
        this.tabId = tabId;
        this.dateTime = dateTime;
        this.user = new User(context);
    }

    @SuppressLint("CheckResult")
    public void getTimeRestoran(){
        Log.d("RestoranInfo", "tabId= " + tabId + " restoranId = " + restoranId + " dateTime = " + dateTime.toString());
        service.getTableOrder(tabId , restoranId, dateTime.toString())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                        about->{
                            getView().setAbout(about);
                        },
                        throwable ->{

                        }
                );
    }

    @SuppressLint("CheckResult")
    public void getRestoranTable(){
        getView().loadingTableList();
        service.getTableRestoran(restoranId, dateTime.toString())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                        res -> getView().updateTableList(res.getTables()),
                        throwable -> {}
                );
    }
}
