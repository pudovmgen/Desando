package com.android.desando.app.ui.restoran.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.android.desando.app.R;
import com.android.desando.app.ui.restoran.model.RestoranTableBean;
import com.android.desando.app.utilities.adapter.BaseAdapterRecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RestoranTablesAdapter extends BaseAdapterRecyclerView<RestoranTableBean, RestoranTablesAdapter.RestoranTableHolder> {
    AdapterView.OnItemClickListener itemClickListener;
    @Override
    protected RestoranTableHolder onCreateHolder(ViewGroup viewGroup, int viewType) {
        return new RestoranTableHolder(LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_restoran_table, viewGroup, false));
    }

    @Override
    protected void onBindHolder(RestoranTableHolder holder, int position) {
        RestoranTableBean model = getItem(position);

        Glide.with(holder.itemView.getContext())
                .load(model.getPhoto())
                .error(R.drawable.image_not_available)
                .into(holder.imageTable);

        holder.itemView.setOnClickListener((v)->itemClickListener.onItemClick(null,null,position,0));
    }

    class RestoranTableHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.res_image_table)
        ImageView imageTable;

        public RestoranTableHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener listener){
        itemClickListener = listener;
    }
}
