package com.android.desando.app.ui.profile.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

public class ResponseCities {
    @SerializedName("cities")
    @Expose
    private List<CityBean> cities = null;
    private final static long serialVersionUID = 5682219382395264979L;

    public List<CityBean> getCities() {
        return cities;
    }

    public void setCities(List<CityBean> cities) {
        this.cities = cities;
    }

    public List<String> getArrayCities(){
        ArrayList<String> listCity = new ArrayList<>();
        for(int i=0;i<cities.size();i++) {
            listCity.add(cities.get(i).getName());
        }
        return listCity;
    }
}
