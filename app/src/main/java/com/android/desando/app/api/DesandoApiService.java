package com.android.desando.app.api;


import com.android.desando.app.ui.catalog.model.FeaturesBean;
import com.android.desando.app.ui.catalog.model.RestoranBean;
import com.android.desando.app.ui.history.model.HistoryInfoResponse;
import com.android.desando.app.ui.history.model.OrderStatusBean;
import com.android.desando.app.ui.review_restoran.model.OrderBodyBean;
import com.android.desando.app.ui.review_restoran.model.OrderTableAbout;
import com.android.desando.app.ui.profile.model.ResponseCities;
import com.android.desando.app.ui.restoran.model.RestoranInfo;
import com.android.desando.app.ui.restoran.model.RestoranTablesResponse;
import com.android.desando.app.ui.signin.model.CheckPhoneBean;
import com.android.desando.app.ui.signin.model.PhoneConfirmBean;
import com.android.desando.app.ui.signin.model.PhoneConfirmResponse;
import com.android.desando.app.ui.signin.model.SignInBean;
import com.android.desando.app.ui.signin.model.SignInUserBean;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

public interface DesandoApiService {
    //Авторизация

    @POST("/api/signin")
    Flowable<CheckPhoneBean> signIn(@Body SignInBean sign); //SignInResponse

    @POST("/api/signin/confirm")
    Flowable<PhoneConfirmResponse> setPhoneConfirm(@Body PhoneConfirmBean confirm);

    @POST("/api/signin/update")
    Flowable<ResponseBody> setPhoneUpdate(@Header("Authorization") String token,
                                          @Body SignInUserBean user);

    //

    @GET("/mobile/cities")
    Flowable<ResponseCities> getCities();

    @GET("/mobile/restorans")
    Flowable<List<RestoranBean>> getRestorans(@Query("city_id") String cityId);

    @GET("/mobile/features")
    Flowable<List<FeaturesBean>> getFeatures(@Query("city_id") String cityId);

    @GET("/mobile/features/{features}")
    Flowable<List<RestoranBean>> getFeaturesRestorans(@Path("features") String features,
                                                      @Query("city_id") String cityId);

    @GET("/mobile/restoran/tables")
    Flowable<RestoranTablesResponse> getTableRestoran(@Query("id") String restoranId,
                                                      @Query("date") String date);

    @GET("/mobile/restoran/search")
    Flowable<List<RestoranBean>> getSearch(@Query("city") String cityId,
                                                      @Query("char") String s,
                                                      @Query("offset") int offset,
                                                      @Query("limit") int limit);

    @GET("/mobile/restoran/info")
    Flowable<RestoranInfo> getRestoran(@Query("id") String restoranId,
                                       @Query("date") String date);

    @GET("/mobile/restoran/table/reservation")
    Flowable<OrderTableAbout> getTableOrder(@Query("id") String tableId,
                                            @Query("restoran_id") String restoranId,
                                            @Query("date") String date);


    @Headers("Content-Type: application/json")
    @POST("/mobile/restoran/table")
    Flowable<ResponseBody> setTableOrder(@Header("Authorization") String token,
                                         @Body OrderBodyBean order);

    @GET("/mobile/restoran/reservations")
    Flowable<HistoryInfoResponse> getHistory(@Header("Authorization") String token);

    @GET("/mobile/restoran/reservation")
    Flowable<OrderStatusBean> getStatusOrder(@Header("Authorization") String token,
                                             @Query("id") String orderId);

    @Streaming
    @GET
    Observable<ResponseBody> downloadPdf(@Url String fileUrl);
}
