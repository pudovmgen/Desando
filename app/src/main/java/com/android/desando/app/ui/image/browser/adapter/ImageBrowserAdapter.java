package com.android.desando.app.ui.image.browser.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.android.desando.app.ui.image.browser.fragment.ImageItemFragment;
import com.android.desando.app.ui.restoran.model.RestoranInfo;

import java.util.List;

import static com.android.desando.app.ui.image.browser.fragment.ImageItemFragment.ARG_IMAGE;

public class ImageBrowserAdapter extends FragmentPagerAdapter {
    List<RestoranInfo.Photo> photos;

    public ImageBrowserAdapter(FragmentManager fm, List<RestoranInfo.Photo> photos) {
        super(fm);
        this.photos = photos;
    }

    @Override
    public Fragment getItem(int position) {
        ImageItemFragment fragment = new ImageItemFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_IMAGE, photos.get(position).getLink());
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getCount() {
        return photos.size();
    }

}