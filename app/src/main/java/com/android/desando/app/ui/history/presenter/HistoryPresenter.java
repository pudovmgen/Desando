package com.android.desando.app.ui.history.presenter;

import android.annotation.SuppressLint;
import android.util.Log;

import com.android.desando.app.api.DesandoApiService;
import com.android.desando.app.ui.history.view.HistoryView;
import com.android.desando.app.utilities.app.User;
import com.android.desando.app.utilities.mvp.BasePresenter;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class HistoryPresenter extends BasePresenter<HistoryView>  {
    DesandoApiService service;
    User user;

    public HistoryPresenter(DesandoApiService service, User user) {
        this.service = service;
        this.user =user;
    }

    @SuppressLint("CheckResult")
    public void getHistory(){
        Log.d("Token",user.getToken());
        service.getHistory(user.getToken())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                        hist-> {
                            getView().setList(hist.getElements());
                                if (hist.getPageInfo().getElementsCount() == 0){
                                    getView().hideList();
                                }
                        },
                        error->{}
                );
    }






}
