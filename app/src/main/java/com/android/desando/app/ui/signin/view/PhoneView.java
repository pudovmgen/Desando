package com.android.desando.app.ui.signin.view;

import com.android.desando.app.utilities.mvp.MvpView;

public interface PhoneView extends MvpView {
    void enableSendingNumber();

    void disableSendingNumber();

    void checkNumber(String s);

    void sendNumber();

    void openConfirm();

    void showProgress();

    void hideProgress();
}
