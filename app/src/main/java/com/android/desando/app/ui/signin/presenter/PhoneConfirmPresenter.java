package com.android.desando.app.ui.signin.presenter;

import android.annotation.SuppressLint;
import android.util.Log;

import com.android.desando.app.api.DesandoApiService;
import com.android.desando.app.ui.signin.model.PhoneConfirmBean;
import com.android.desando.app.ui.signin.model.SignInBean;
import com.android.desando.app.ui.signin.model.SignInUserBean;
import com.android.desando.app.ui.signin.view.PhoneConfirmView;
import com.android.desando.app.ui.signin.view.PhoneView;
import com.android.desando.app.utilities.app.User;
import com.android.desando.app.utilities.mvp.BasePresenter;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class PhoneConfirmPresenter extends BasePresenter<PhoneConfirmView>  {
    DesandoApiService service;
    private String phone;
    public User user;


    public PhoneConfirmPresenter(DesandoApiService service, String phone, User user) {
        this.service = service;
        this.phone = phone;
        this.user = user;
    }

    @SuppressLint("CheckResult")
    public void confirmPhone(String code){
        service.setPhoneConfirm(new PhoneConfirmBean(phone, code))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                        success->{
                            getView().successfulAuth(success.getToken());
                        },
                        throwable ->{
                            Log.d("API", throwable.getMessage());
                        }
                );
    }

    @SuppressLint("CheckResult")
    public void updatePhone(String token){
        service.setPhoneUpdate("Bearer " +token,
                new SignInUserBean(user.getName(),user.getCityName()))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                        success->{
                            getView().successfulUpdate();
                        },
                        throwable ->{
                            Log.d("API", throwable.getMessage());
                        }
                );
    }

}
