package com.android.desando.app.ui.review_restoran.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.android.desando.app.R;

import butterknife.BindView;
import butterknife.OnClick;

public class ConfirmPhoneDialog extends Dialog {

    @BindView(R.id.confirmPhoneNumberDialogCancelBtn)
    Button cancelBtn;
    @BindView(R.id.confirmPhoneNumberDialogOkBtn)
    Button okBtn;

    View.OnClickListener callback;


    public ConfirmPhoneDialog(@NonNull Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_confirm_phone);
    }

    @OnClick({R.id.confirmPhoneNumberDialogCancelBtn, R.id.confirmPhoneNumberDialogOkBtn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.confirmPhoneNumberDialogCancelBtn:
                dismiss();
                break;
            case R.id.confirmPhoneNumberDialogOkBtn:
                callback.onClick(view);
                dismiss();
                break;
        }
    }

    public ConfirmPhoneDialog setOnClickOkLianer(View.OnClickListener lianer){
        callback = lianer;
        return this;
    }
}

