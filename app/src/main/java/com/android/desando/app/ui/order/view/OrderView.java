package com.android.desando.app.ui.order.view;

import com.android.desando.app.ui.review_restoran.model.OrderTableAbout;
import com.android.desando.app.utilities.mvp.MvpView;

public interface OrderView extends MvpView {

    void showLayout();

    void hideLayout();

    void setAbout(OrderTableAbout about);

    void openImage();

    void chooseDate();

    void chooseTime(String time);

    void checkUserAuth();

    void messageOrder();

    void messageSelectTimeOrder();

    void tableOrder();

    void successOrder();

    void ErrorOrder(String s);

    void openDialogMens();

}