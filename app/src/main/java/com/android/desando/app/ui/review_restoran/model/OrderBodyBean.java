package com.android.desando.app.ui.review_restoran.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class OrderBodyBean implements Serializable
{
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("restoran")
    @Expose
    private String restoran;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("comment")
    @Expose
    private String comment;
    private final static long serialVersionUID = 1230772801091808042L;

    public OrderBodyBean(String id, String restoran, String time, String comment) {
        this.id = id;
        this.restoran = restoran;
        this.time = time;
        this.comment = comment;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRestoran() {
        return restoran;
    }

    public void setRestoran(String restoran) {
        this.restoran = restoran;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}