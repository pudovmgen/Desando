package com.android.desando.app.ui.restoran.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RestoranTableBean implements Serializable {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("inner")
    @Expose
    private Integer inner;
    @SerializedName("number")
    @Expose
    private String number;
    @SerializedName("description")
    @Expose
    private Object description;
    @SerializedName("photo")
    @Expose
    private String photo;
    private final static long serialVersionUID = 1767689626591172803L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getInner() {
        return inner;
    }

    public void setInner(Integer inner) {
        this.inner = inner;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getDescription() {
        return (String) description;
    }

    public void setDescription(Object description) {
        this.description = description;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
