package com.android.desando.app.ui.profile.presenter;

import android.annotation.SuppressLint;
import android.util.Log;

import com.android.desando.app.api.DesandoApiService;
import com.android.desando.app.ui.profile.model.ResponseCities;
import com.android.desando.app.ui.profile.view.ProfileView;
import com.android.desando.app.utilities.mvp.BasePresenter;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class ProfilePresenter extends BasePresenter<ProfileView> {
    public ResponseCities listCity = null;
    DesandoApiService service;

    public ProfilePresenter(DesandoApiService service) {
        this.service = service;
    }

    @SuppressLint("CheckResult")
    public void getCities(){
        service.getCities()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                        cities -> {
                            getView().setAdapterSpinner(
                                    cities.getArrayCities(),
                                    cities.getCities().get(0).getId(),
                                    cities.getCities().get(0).getName()
                            );
                            listCity = cities;
                        },
                        throwable ->{}
                );
    }
}
