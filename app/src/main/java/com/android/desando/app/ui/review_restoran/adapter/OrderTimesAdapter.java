package com.android.desando.app.ui.review_restoran.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.desando.app.R;
import com.android.desando.app.utilities.adapter.BaseAdapterRecyclerView;

import java.io.Serializable;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderTimesAdapter extends BaseAdapterRecyclerView<String, OrderTimesAdapter.RestoranTableHolder> {
    OrderTimesCallback itemClickListener;
    private int selectTime = -1;
    @Override
    protected RestoranTableHolder onCreateHolder(ViewGroup viewGroup, int viewType) {
        return new RestoranTableHolder(LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_order_time, viewGroup, false));
    }

    @Override
    protected void onBindHolder(RestoranTableHolder holder, int position) {
        String time = getItem(position);
        holder.lableTime.setText(time);


        if(position == selectTime){
            holder.timeBtn.setBackgroundResource(R.drawable.btn_switch_time_select);
        }else {
            holder.timeBtn.setBackgroundResource(R.drawable.btn_switch_time);
        }

        holder.itemView.setOnClickListener(v -> {
            if (getItem(position).equals("→")){
                itemClickListener.nextPageAdapter();
                return;
            }
            if (getItem(position).equals("←")){
                itemClickListener.backPageAdapter();
                return;
            }
            if (selectTime !=-1) {
                notifyItemChanged(selectTime);
                notifyItemChanged(position);
            }else {
                selectTime = position;
                notifyItemChanged(position);
            }
            selectTime = position;
            itemClickListener.selectAdapterTime(time);
        });
    }

    class RestoranTableHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.lable_time)
        TextView lableTime;
        @BindView(R.id.time_btn)
        LinearLayout timeBtn;

        public RestoranTableHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void setOnItemClickListener(OrderTimesCallback listener) {
        itemClickListener = listener;
    }

    public int getSelectTime() {
        return selectTime;
    }

    public interface OrderTimesCallback extends Serializable{
        void selectAdapterTime(String time);

        void nextPageAdapter();

        void backPageAdapter();
    }
}
