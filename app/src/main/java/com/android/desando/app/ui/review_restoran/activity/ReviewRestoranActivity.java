package com.android.desando.app.ui.review_restoran.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.desando.app.R;
import com.android.desando.app.api.DesandoApiService;
import com.android.desando.app.ui.review_restoran.adapter.OrderTimesAdapter;
import com.android.desando.app.ui.review_restoran.adapter.TimePagerAdapter;
import com.android.desando.app.ui.review_restoran.model.OrderTableAbout;
import com.android.desando.app.ui.review_restoran.presenter.OrderPresenter;
import com.android.desando.app.ui.restoran.adapter.RestoranTablesAdapter;
import com.android.desando.app.ui.restoran.model.RestoranDateTime;
import com.android.desando.app.ui.restoran.model.RestoranTableBean;
import com.android.desando.app.ui.review_restoran.view.ReviewRestoranView;
import com.android.desando.app.utilities.activity.DesandoActivity;
import com.android.desando.app.utilities.app.ApplicationApp;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.OnClick;

import static com.android.desando.app.ui.restoran.activity.RestoranActivity.ARG_RESTORAN_ID;

public class ReviewRestoranActivity extends DesandoActivity implements ReviewRestoranView, OrderTimesAdapter.OrderTimesCallback {
    public static String ARG_TABLE_ID = "tableId";
    public static String ARG_TABLE_DATE = "tableDate";

    OrderPresenter presenter;
    @BindView(R.id.res_name)
    TextView resName;
    @BindView(R.id.order_date)
    TextView orderDate;
    @BindView(R.id.tab_capacity)
    TextView tabCapacity;

    @BindView(R.id.constraintLayout)
    ConstraintLayout maiLayout;
    @BindView(R.id.scroll)
    NestedScrollView scroll;

    @Inject
    DesandoApiService service;

    TimePagerAdapter adapter;
    @BindView(R.id.order_load)
    ConstraintLayout orderLoad;

    @BindView(R.id.txt_mens_count)
    TextView mensCount;

    @BindString(R.string.order_lable_men_count)
    String titleMenCount;
    @BindString(R.string.order_lable_men_null)
    String titleMenNull;
    @BindView(R.id.date_pager)
    ViewPager datePager;
    @BindView(R.id.table_online)
    RecyclerView tableOnline;

    RestoranTablesAdapter adapterTable;
    @BindView(R.id.progress_table)
    ProgressBar progressTable;
    @BindView(R.id.order_common_time)
    TextView orderCommonTime;


    @Override
    protected void initToolbar() {
        navigationLeft(true);
        setLeftNavigationListener(v -> ReviewRestoranActivity.this.finish());
    }

    @Override
    protected int initContentView() {
        return R.layout.activity_reservation;
    }

    @Override
    protected void initView() {
        hideLayout();
        adapter = new TimePagerAdapter(getSupportFragmentManager(), this);
        adapterTable = new RestoranTablesAdapter();

        tableOnline.setNestedScrollingEnabled(false);
        tableOnline.setAdapter(adapterTable);
        adapterTable.setOnItemClickListener((parent, view, position, id) -> openOrder(adapterTable.getItem(position).getId()));

        datePager.setAdapter(adapter);

        presenter.getTimeRestoran();

        orderDate.setOnClickListener(v -> chooseDate());

        mensCount.setText(titleMenCount);

        hideTableList();
    }

    @Override
    protected void initDaggerInject() {
        ((ApplicationApp)
                this.getApplication())
                .getComponent()
                .inject(this);

        presenter = new OrderPresenter(service,
                getIntent().getStringExtra(ARG_RESTORAN_ID),
                getIntent().getStringExtra(ARG_TABLE_ID),
                (RestoranDateTime) getIntent().getSerializableExtra(ARG_TABLE_DATE),
                this
        );
        presenter.attachView(this);

    }

    @Override
    public void showLayout() {
        orderLoad.setVisibility(View.GONE);
        maiLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLayout() {
        orderLoad.setVisibility(View.VISIBLE);
        maiLayout.setVisibility(View.GONE);
    }

    @Override
    public void setAbout(OrderTableAbout about) {
        resName.setText(about.getName());
        orderDate.setText(presenter.dateTime.getCurrentDate());
        adapter.updateTimes(about.getTime());
        showLayout();
    }

    @Override
    public void chooseDate() {
        DatePickerDialog dialog = new DatePickerDialog(this,
                (view, year, month, dayOfMonth) -> {
                    presenter.dateTime.setDate(dayOfMonth, month, year);
                    // presenter.getTableOrder();
                },
                presenter.dateTime.getYear(),
                presenter.dateTime.getMonth() - 1,
                presenter.dateTime.getDayOfMonth()
        );
        presenter.dateTime.addMonth(1);
        long date =  presenter.dateTime.getDate().getTime();
        presenter.dateTime.removeMonth(1);
        dialog.getDatePicker()
                .setMaxDate(date);
        dialog.show();
    }

    @Override
    public void openDialogMens() {
        String[] counts = {"2", "3", "4", "5", "6", "7", "8", "9", "10"};
        new AlertDialog.Builder(this)
                .setTitle("Колличество человек: ")
                .setItems(counts, (dialog, which) -> {
                    mensCount.setText("Колличество человек: " + counts[which]);
                })
                .create()
                .show();
    }

    @Override
    public void openDialogTimeCount() {
        String[] counts = {"1 Час", "2 Часа", "3 Часа", "4 Часа", "5 Часов", "6 Часов", "7 Часов", "8 Часов", "9 Часов", "10 Часов",
                "11 Часов", "12 Часов"};
        new AlertDialog.Builder(this)
                .setItems(counts, (dialog, which) -> {
                    orderCommonTime.setText(counts[which]);
                })
                .create()
                .show();
    }

    @Override
    public void showTableList() {
        progressTable.setVisibility(View.GONE);
        tableOnline.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideTableList() {
        tableOnline.setVisibility(View.GONE);
        progressTable.setVisibility(View.GONE);
    }

    @Override
    public void loadingTableList() {
        hideTableList();
        progressTable.setVisibility(View.VISIBLE);
    }

    @Override
    public void updateTableList(List<RestoranTableBean> tables) {
        showTableList();
        adapterTable.addAllItemsInClear(tables);
    }

    @OnClick({R.id.txt_mens_count, R.id.order_common_time})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.order_common_time:
                openDialogTimeCount();
                break;
            case R.id.txt_mens_count:
                openDialogMens();
                break;
        }
    }

    @Override
    public void selectAdapterTime(String time) {
        String[] t = time.split(":");
        presenter.dateTime.setTime(Integer.parseInt(t[0]), Integer.parseInt(t[1]));
        presenter.getRestoranTable();
    }

    @Override
    public void nextPageAdapter() {
        try {
            datePager.setCurrentItem(datePager.getCurrentItem() + 1);
        } catch (Exception e) {}
    }

    @Override
    public void backPageAdapter() {
        try {
            datePager.setCurrentItem(datePager.getCurrentItem() - 1);
        } catch (Exception e) {}
    }

    public void openOrder(String tabId) {
    /*    Intent intent = new Intent(this, OrderActivity.class);
        intent.putExtra(ARG_RESTORAN_ID, presenter.restoranId);
        intent.putExtra(ARG_TABLE_ID, tabId);
        intent.putExtra(ARG_TABLE_DATE, presenter.dateTime);
        startActivity(intent);*/
    }
}
