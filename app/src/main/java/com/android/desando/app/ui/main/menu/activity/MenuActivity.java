package com.android.desando.app.ui.main.menu.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import com.android.desando.app.R;
import com.android.desando.app.ui.catalog.activity.CatalogActivity;
import com.android.desando.app.ui.profile.activity.ProfileActivity;
import com.android.desando.app.utilities.app.User;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.android.desando.app.ui.catalog.activity.CatalogActivity.ARG_CITY_ID;

public class MenuActivity extends AppCompatActivity implements TextWatcher {

    @BindView(R.id.btn_sign_in)
    Button btnSignIn;
    @BindView(R.id.logo)
    TextView logo;
    User user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        Typeface tf = Typeface.createFromAsset(getAssets(), "font/historia_reguler.ttf");
        logo.setTypeface(tf, Typeface.NORMAL);
        user = new User(this);
    }

    @OnClick(R.id.btn_sign_in)
    public void onViewClicked() {
        checkAuth();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    public void checkAuth() {
        User user = new User(this);
        if (user.isName()) {
            Intent intent = new Intent(this, CatalogActivity.class);
            intent.putExtra(ARG_CITY_ID, user.getCityId());
            startActivity(intent);
        } else {
            startActivity(new Intent(this, ProfileActivity.class));
        }
    }

}
