package com.android.desando.app.ui.restoran.presenter;

import android.annotation.SuppressLint;
import android.util.Log;

import com.android.desando.app.api.DesandoApiService;
import com.android.desando.app.ui.restoran.model.RestoranDateTime;
import com.android.desando.app.ui.restoran.view.RestoranView;
import com.android.desando.app.utilities.mvp.BasePresenter;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Action;
import io.reactivex.schedulers.Schedulers;

public class RestoranPresenter extends BasePresenter<RestoranView>  {
    DesandoApiService service;
    public String restoranId;
    public RestoranDateTime dateTime;
    public String memuUrl;

    public RestoranPresenter(DesandoApiService service, String restoranId) {
        this.service = service;
        this.restoranId =restoranId;
        dateTime = new RestoranDateTime(Calendar.getInstance());
    }

    @SuppressLint({"CheckResult", "SimpleDateFormat"})
    public void getRestoranAbout(){
        service.getRestoran(restoranId, new SimpleDateFormat("ddMMyyyyHHmm").format(dateTime.getDate()))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                        getView()::setRestoranAbout,
                        throwable ->{}
                );
    }


    public String getCurrentDate(){
       return dateTime.getCurrentDate();
   }

    public String getCurrentTime(){
        return dateTime.getCurrentTime();
   }

   private void parseWorkTime(String workTime){

   }

}
