package com.android.desando.app.ui.review_restoran.view;

import com.android.desando.app.ui.review_restoran.model.OrderTableAbout;
import com.android.desando.app.ui.restoran.model.RestoranTableBean;
import com.android.desando.app.utilities.mvp.MvpView;

import java.util.List;

public interface ReviewRestoranView extends MvpView {
    void showLayout();

    void hideLayout();

    void setAbout(OrderTableAbout about);

    void chooseDate();

    void openDialogMens();

    void openDialogTimeCount();

    void showTableList();

    void hideTableList();

    void loadingTableList();

    void updateTableList(List<RestoranTableBean> tables);

}
