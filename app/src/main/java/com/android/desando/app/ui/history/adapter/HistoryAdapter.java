package com.android.desando.app.ui.history.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.android.desando.app.R;
import com.android.desando.app.ui.catalog.model.FeaturesBean;
import com.android.desando.app.ui.history.model.HistoryBean;
import com.android.desando.app.utilities.adapter.BaseAdapterRecyclerView;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

public class HistoryAdapter extends BaseAdapterRecyclerView<HistoryBean, HistoryAdapter.HistoryHolder> {

    @Override
    protected HistoryHolder onCreateHolder(ViewGroup viewGroup, int viewType) {
        return new HistoryHolder(LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_history, viewGroup, false));
    }

    @Override
    protected void onBindHolder(HistoryHolder holder, int position) {
        HistoryBean model = getItem(position);
        holder.orderDate.setText(model.getDate());
        holder.resName.setText(model.getRestoran());
        holder.resAddress.setText(model.getAddress());
        holder.orderComment.setText(holder.commentFormat+ model.getComment());
        holder.orderStatus.setText(model.getStatus());

        Glide.with(holder.itemView.getContext())
                .load(model.getImage())
                .into(holder.resImage);

        holder.itemView.setOnClickListener(v->callbackOnSelected.onItemSelected(model,position));
    }


    class HistoryHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.res_image)
        ImageView resImage;
        @BindView(R.id.order_date)
        TextView orderDate;
        @BindView(R.id.res_name)
        TextView resName;
        @BindView(R.id.res_address)
        TextView resAddress;
        @BindView(R.id.order_comment)
        TextView orderComment;
        @BindView(R.id.order_status)
        TextView orderStatus;
        @BindString(R.string.history_comment)
        String commentFormat;

        public HistoryHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}
