package com.android.desando.app.utilities.di;

import com.android.desando.app.ui.catalog.activity.CatalogActivity;
import com.android.desando.app.ui.history.activity.HistoryActivity;
import com.android.desando.app.ui.history.activity.OpenOrderHistoryActivity;
import com.android.desando.app.ui.pdf.browser.activity.PdfBrowserActivity;
import com.android.desando.app.ui.profile.activity.ProfileActivity;
import com.android.desando.app.ui.restoran.activity.RestoranActivity;
import com.android.desando.app.ui.review_restoran.activity.ReviewRestoranActivity;
import com.android.desando.app.ui.signin.activity.PhoneConfirmActivity;
import com.android.desando.app.ui.signin.activity.SignInActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {NetModule.class})
public interface AppComponent {
    void inject(CatalogActivity activity);
    void inject(ProfileActivity activity);
    void inject(RestoranActivity activity);
    void inject(ReviewRestoranActivity activity);
    //void inject(OrderActivity activity);
    void inject(SignInActivity activity);
    void inject(PhoneConfirmActivity activity);
    void inject(HistoryActivity activity);
    void inject(OpenOrderHistoryActivity activity);
    void inject(PdfBrowserActivity activity);
}
