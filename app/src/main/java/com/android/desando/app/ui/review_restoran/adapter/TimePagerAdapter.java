package com.android.desando.app.ui.review_restoran.adapter;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.android.desando.app.ui.review_restoran.fragment.DateFragment;
import com.google.common.collect.Lists;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static com.android.desando.app.ui.review_restoran.fragment.DateFragment.ARG_CALLBACK;
import static com.android.desando.app.ui.review_restoran.fragment.DateFragment.ARG_TIMES_LIST;

public class TimePagerAdapter extends FragmentPagerAdapter {
    //ArrayList<String> freeTimes;
    List<Fragment> adapterTimes;
    final int ITEM_COUNT_PAGE = 15;
    OrderTimesAdapter.OrderTimesCallback callback;


    public TimePagerAdapter(FragmentManager fm, OrderTimesAdapter.OrderTimesCallback callback) {
        super(fm);
        this.callback =callback;
        adapterTimes = new ArrayList<>();
    }

    public void updateTimes(List<String> times){

        for (int i=0, j = 0; i < times.size(); i++){
            if (i % ITEM_COUNT_PAGE == 0 && i !=0 ){
                DateFragment fragment = new DateFragment();
                Bundle arg = new Bundle();
                List<String> test = Lists.newArrayList(times.subList(j, i));
                if (i + ITEM_COUNT_PAGE < times.size()) test.add("→");
                if (j > 0) test.add(0, "←");

                String[] array = test.toArray(new String[test.size()]);
                arg.putStringArray(ARG_TIMES_LIST, array);
                arg.putSerializable(ARG_CALLBACK, (Serializable) callback);
                fragment.setArguments(arg);
                adapterTimes.add(fragment);
                j = i;
            }
        }

        notifyDataSetChanged();
    }

    @Override
    public Fragment getItem(int position) {
        return adapterTimes.get(position);
    }

    @Override
    public int getCount() {
        return adapterTimes.size();
    }
}
