package com.android.desando.app.ui.history.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.android.desando.app.R;
import com.android.desando.app.api.DesandoApiService;
import com.android.desando.app.ui.history.model.OrderStatusBean;
import com.android.desando.app.utilities.activity.DesandoActivity;
import com.android.desando.app.utilities.app.ApplicationApp;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class OpenOrderHistoryActivity extends DesandoActivity {
    public static String ARG_ORDER_BEAN = "orderItem";
    public static String ARG_ORDER_ID = "orderId";
    @BindView(R.id.order_image)
    public ImageView orderImage;
    @BindView(R.id.order_status)
    TextView orderStatus;
    @BindView(R.id.order_res)
    TextView orderRes;
    @BindView(R.id.order_addres)
    TextView orderAddres;
    @BindView(R.id.order_date)
    TextView orderDate;
    @BindView(R.id.order_time)
    TextView orderTime;
    @BindView(R.id.order_tab)
    TextView orderTab;
    @BindView(R.id.progress)
    ProgressBar progress;
    @BindView(R.id.root)
    ConstraintLayout root;
    @Inject
    DesandoApiService service;
    @BindView(R.id.order_status_label)
    TextView orderStatusLabel;
    @BindView(R.id.order_status_label2)
    TextView orderStatusLabel2;
    @BindView(R.id.order_status_label3)
    TextView orderStatusLabel3;


    @Override
    protected void initToolbar() {
        navigationLeft(true);
        setLeftNavigationListener(v -> OpenOrderHistoryActivity.this.finish());
    }


    @Override
    protected int initContentView() {
        return R.layout.activity_order_waiting_confirmation;
    }

    @SuppressLint("CheckResult")
    @Override
    protected void initView() {
        String id = getIntent().getStringExtra(ARG_ORDER_ID);
        showLoad();
        service.getStatusOrder(getUser().getToken(), id)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                        order -> {
                            setView(order);
                            hideLoad();
                        },
                        throwable -> {
                        }
                );

    }

    private void showLoad() {
        progress.setVisibility(View.VISIBLE);
        root.setVisibility(View.GONE);
    }

    private void hideLoad() {
        progress.setVisibility(View.GONE);
        root.setVisibility(View.VISIBLE);
    }

    private void setView(OrderStatusBean order) {
        orderStatus.setText(order.getDescription());
        orderRes.setText(order.getRestoran());
        orderAddres.setText(order.getAddress());
        orderDate.setText(order.getOrderDate());
        orderTime.setText(order.getOrderTime());
        orderTab.setText(order.getTable() + "");

        if(order.getField1() !=null)
            orderStatusLabel.setText(order.getField1());
        else
            orderStatusLabel.setVisibility(View.GONE);

        if(order.getField2() !=null)
            orderStatusLabel2.setText(order.getField2());
        else
            orderStatusLabel2.setVisibility(View.GONE);

        if(order.getField3() !=null)
            orderStatusLabel3.setText(order.getField3());
        else
            orderStatusLabel3.setVisibility(View.GONE);

        Glide.with(this)
                .load(order.getPhoto())
                .into(orderImage);
    }

    @Override
    protected void initDaggerInject() {
        ((ApplicationApp)
                this.getApplication())
                .getComponent()
                .inject(this);
    }

}
