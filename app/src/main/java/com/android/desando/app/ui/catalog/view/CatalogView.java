package com.android.desando.app.ui.catalog.view;

import com.android.desando.app.ui.catalog.model.FeaturesBean;
import com.android.desando.app.ui.catalog.model.RestoranBean;
import com.android.desando.app.utilities.mvp.MvpView;

import java.util.List;

public interface CatalogView extends MvpView {
    //Restorans
    void hideRestoransList();

    void showRestoransList();

    void setRestoransList(List<RestoranBean> items);
    ///

    //Tags
    void hideFeaturesImage();

    void showFeaturesImage();

    void setTagList(List<FeaturesBean> tags);

    void setFeatures(FeaturesBean features);
    //
    void hideSearch();

    void showSearch();
    //

    void openRestoran(String restoranId);

    void openDialogAuth();

    void openFeatures(FeaturesBean features);

    void openAuth();

    void search(String s);

}
