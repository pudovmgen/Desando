package com.android.desando.app.utilities.app;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class User {
    SharedPreferences preferences;
    SharedPreferences.Editor ed;
    private String PREF_USER_DATA  = "USER";
    private String PREF_USER_TOKEN = "TOKEN";
    private String PREF_USER_PHONE = "PHONE";
    private String PREF_USER_NAME =  "USER_NAME";
    private String PREF_USER_CITY =  "USER_CITY";
    private String PREF_USER_CITY_ID =  "USER_CITY_ID";

    public User(Context context) {
        preferences = context.getSharedPreferences(PREF_USER_DATA, Context.MODE_PRIVATE);
        ed = preferences.edit();
    }

    public void setToken(String token){
        ed.putString(PREF_USER_TOKEN, token);
        ed.commit();
    }

    public boolean isAuth(){
       return preferences.contains(PREF_USER_TOKEN);
    }

    public boolean isName(){
        return preferences.contains(PREF_USER_NAME);
    }


    public void setCity(String city, String cityId){
        ed.putString(PREF_USER_CITY, city);
        ed.putString(PREF_USER_CITY_ID, cityId);
        ed.commit();
    }

    public void setName(String name){
        ed.putString(PREF_USER_NAME, name);
        ed.commit();
    }

    public void setPhone(String phone){
        ed.putString(PREF_USER_PHONE, phone);
        ed.commit();
    }

    public String getToken(){
        String token = preferences.getString(PREF_USER_TOKEN, null);
        return "Bearer " + token;
    }

    public String getCityName(){
        return preferences.getString(PREF_USER_CITY, "-");
    }

    public String getCityId(){
        return preferences.getString(PREF_USER_CITY_ID, "");
    }

    public String getName(){
        return preferences.getString(PREF_USER_NAME, "-");
    }

    public String getPhone(){
        return preferences.getString(PREF_USER_PHONE, "-");
    }

}
