package com.android.desando.app.ui.history.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class HistoryInfo implements Serializable
{

    @SerializedName("nextOffset")
    @Expose
    private Integer nextOffset;
    @SerializedName("elementsCount")
    @Expose
    private Integer elementsCount;
    @SerializedName("limit")
    @Expose
    private Integer limit;
    @SerializedName("totalElements")
    @Expose
    private Integer totalElements;
    @SerializedName("totalPages")
    @Expose
    private Integer totalPages;
    @SerializedName("isLast")
    @Expose
    private Boolean isLast;
    private final static long serialVersionUID = 4005366753790450177L;

    public Integer getNextOffset() {
        return nextOffset;
    }

    public void setNextOffset(Integer nextOffset) {
        this.nextOffset = nextOffset;
    }

    public Integer getElementsCount() {
        return elementsCount;
    }

    public void setElementsCount(Integer elementsCount) {
        this.elementsCount = elementsCount;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getTotalElements() {
        return totalElements;
    }

    public void setTotalElements(Integer totalElements) {
        this.totalElements = totalElements;
    }

    public Integer getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }

    public Boolean getIsLast() {
        return isLast;
    }

    public void setIsLast(Boolean isLast) {
        this.isLast = isLast;
    }

}