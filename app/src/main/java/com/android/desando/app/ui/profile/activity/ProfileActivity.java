package com.android.desando.app.ui.profile.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;

import com.android.desando.app.R;
import com.android.desando.app.api.DesandoApiService;
import com.android.desando.app.ui.catalog.activity.CatalogActivity;
import com.android.desando.app.ui.profile.presenter.ProfilePresenter;
import com.android.desando.app.ui.profile.view.ProfileView;
import com.android.desando.app.utilities.activity.DesandoActivity;
import com.android.desando.app.utilities.app.ApplicationApp;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

import static com.android.desando.app.ui.catalog.activity.CatalogActivity.ARG_CITY_ID;

public class ProfileActivity extends DesandoActivity implements ProfileView, AdapterView.OnItemSelectedListener {
    @BindView(R.id.editText)
    EditText editText;
    @BindView(R.id.spinner)
    Spinner spinner;
    @BindView(R.id.btn_next)
    Button btnNext;
    @BindView(R.id.view)
    View separator;
    @BindView(R.id.progress)
    ProgressBar progress;

    @Inject
    DesandoApiService service;

    ProfilePresenter presenter;
    String cityId;
    String cityName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected int initContentView() {
        return R.layout.activity_profile;
    }

    @Override
    protected void initView() {
        spinner.setOnItemSelectedListener(this);
        hideList();
        presenter.getCities();
    }

    @Override
    protected void initToolbar() {
        navigationLeft(true);
        setLeftNavigationListener(v -> ProfileActivity.this.finish());
    }

    @Override
    protected void initDaggerInject() {
        ((ApplicationApp)
                this.getApplication())
                .getComponent()
                .inject(this);
        presenter = new ProfilePresenter(service);
        presenter.attachView(this);
    }


    @Override
    public void setAdapterSpinner(List<String> items, String idDefault, String nameDefault) {
        cityId = idDefault;
        cityName = nameDefault;
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        showList();
    }

    @Override
    public void hideList() {
        spinner.setVisibility(View.GONE);
        separator.setVisibility(View.GONE);
        showLoad();
    }

    @Override
    public void showList() {
        spinner.setVisibility(View.VISIBLE);
        separator.setVisibility(View.VISIBLE);
        hideLoad();
    }

    @Override
    public void showLoad() {
        progress.setVisibility(View.VISIBLE);
        btnNext.setVisibility(View.GONE);
    }

    @Override
    public void hideLoad() {
        progress.setVisibility(View.GONE);
        btnNext.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.btn_next)
    public void onViewClicked(View view) {
        getUser().setCity(cityName, cityId);
        getUser().setName(editText.getText()+"");

        Intent intent = new Intent(this, CatalogActivity.class);
        intent.putExtra(ARG_CITY_ID, cityId);
        startActivity(intent);
        ProfileActivity.this.finish();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        cityId = presenter.listCity.getCities().get(position).getId();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
