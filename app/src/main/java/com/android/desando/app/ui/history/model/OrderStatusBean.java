package com.android.desando.app.ui.history.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class OrderStatusBean implements Serializable
{

    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("field_1")
    @Expose
    private String field1;
    @SerializedName("field_2")
    @Expose
    private String field2;
    @SerializedName("field_3")
    @Expose
    private String field3;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("table")
    @Expose
    private Integer table;
    @SerializedName("people")
    @Expose
    private Integer people;
    @SerializedName("restoran")
    @Expose
    private String restoran;
    @SerializedName("photo")
    @Expose
    private String photo;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("address")
    @Expose
    private String address;
    private final static long serialVersionUID = 5611153271410509056L;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getField1() {
        return field1;
    }

    public void setField1(String field1) {
        this.field1 = field1;
    }

    public String getField2() {
        return field2;
    }

    public void setField2(String field2) {
        this.field2 = field2;
    }

    public String getField3() {
        return field3;
    }

    public void setField3(String field3) {
        this.field3 = field3;
    }

    public String getDate() {
        return date;
    }

    public String getOrderDate(){
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
            return new SimpleDateFormat("dd MMMM yyyy").format(dateFormat.parse(date));
        } catch (ParseException e) {return "";}
    }

    public String getOrderTime(){
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
            return new SimpleDateFormat("HH:mm").format(dateFormat.parse(date));
        } catch (ParseException e) {return "";}
    }

    public Integer getTable() {
        return table;
    }

    public void setTable(Integer table) {
        this.table = table;
    }

    public Integer getPeople() {
        return people;
    }

    public void setPeople(Integer people) {
        this.people = people;
    }

    public String getRestoran() {
        return restoran;
    }

    public void setRestoran(String restoran) {
        this.restoran = restoran;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

}