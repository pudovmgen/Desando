package com.android.desando.app.ui.history.view;

import com.android.desando.app.ui.history.model.HistoryBean;
import com.android.desando.app.utilities.mvp.MvpView;

import java.util.List;

public interface HistoryView extends MvpView {

    void showList();

    void hideList();

    void showEmpty();

    void setList(List<HistoryBean> history);

    void openOrder(HistoryBean order);
}
