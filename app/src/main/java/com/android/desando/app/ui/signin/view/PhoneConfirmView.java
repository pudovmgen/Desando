package com.android.desando.app.ui.signin.view;

import com.android.desando.app.utilities.mvp.MvpView;

public interface PhoneConfirmView extends MvpView {
    void showProgress();

    void hideProgress();

    void checkCode(String c);

    void successfulAuth(String token);

    void successfulUpdate();
}
