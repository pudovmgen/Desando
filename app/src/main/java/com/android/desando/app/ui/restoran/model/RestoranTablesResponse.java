package com.android.desando.app.ui.restoran.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class RestoranTablesResponse implements Serializable {

    @SerializedName("tables")
    @Expose
    private List<RestoranTableBean> tables = null;
    private final static long serialVersionUID = -1176719141575354248L;

    public List<RestoranTableBean> getTables() {
        return tables;
    }

    public void setTables(List<RestoranTableBean> tables) {
        this.tables = tables;
    }

}

