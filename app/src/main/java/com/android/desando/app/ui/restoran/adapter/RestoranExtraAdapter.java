package com.android.desando.app.ui.restoran.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.desando.app.R;
import com.android.desando.app.utilities.adapter.BaseAdapterRecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class RestoranExtraAdapter extends BaseAdapterRecyclerView<String, RestoranExtraAdapter.RestoranExtraHolder> {

    @Override
    protected RestoranExtraHolder onCreateHolder(ViewGroup viewGroup, int viewType) {
        return new RestoranExtraHolder(LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_restoran_additionally, viewGroup, false));
    }

    @Override
    protected void onBindHolder(RestoranExtraHolder holder, int position) {
        holder.extraText.setText(getItem(position));
    }

    class RestoranExtraHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.extra_image)
        CircleImageView extraImage;
        @BindView(R.id.extra_text)
        TextView extraText;

        public RestoranExtraHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
