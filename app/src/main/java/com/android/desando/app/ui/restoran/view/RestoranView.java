package com.android.desando.app.ui.restoran.view;

import com.android.desando.app.ui.restoran.model.RestoranInfo;
import com.android.desando.app.ui.restoran.model.RestoranTableBean;
import com.android.desando.app.utilities.mvp.MvpView;

import java.util.List;

public interface RestoranView extends MvpView {
    void showTableList();

    void hideTableList();

    void showRestoranInfo();

    void hideRestoranInfo();

    void setRestoranAbout(RestoranInfo about);

    void setTableAdapter(List<RestoranTableBean> tables);

    void openMenu();

    void openImage(int pos);

    void openOrder(String tabId);

    void checkAboutEmpty(RestoranInfo about);

    void menuEmpty(String link);

    void tableEmpty(List<RestoranTableBean> tables);

    void galereaEmpty();

    void extraEmpty();

    void billEmpty(String bill);

}
