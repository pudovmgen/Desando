package com.android.desando.app.utilities.activity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.desando.app.R;
import com.android.desando.app.utilities.app.User;

import javax.annotation.Nullable;

import butterknife.BindView;

public abstract class DesandoActivity extends BaseActivity {

    @BindView(R.id.toolbar_title) TextView toolbarTitle;
    @BindView(R.id.left_item) ImageView leftItem;
    @BindView(R.id.right_item) ImageView rightItem;

    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        user = new User(this);
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        toolbarSetting();
        initToolbar();
    }

    protected abstract void initToolbar();

    //Toolbar
    private void toolbarSetting(){
        leftItem.setVisibility(View.GONE);
        rightItem.setVisibility(View.GONE);

        Typeface tf = Typeface.createFromAsset(getAssets(), "font/historia_reguler.ttf");
        toolbarTitle.setTypeface(tf, Typeface.NORMAL);
    }

    public void setTitle(String title){
        toolbarTitle.setText(title);
    }

    public void setTitle(int res){
        toolbarTitle.setText(res);
    }

    public void setLeftNavigationDrawable(int res){
        leftItem.setImageResource(res);
    }

    public void setRightNavigationDrawable(int res){
        rightItem.setImageResource(res);
    }

    public void setLeftNavigationListener(@Nullable View.OnClickListener l){
        leftItem.setOnClickListener(l);
    }

    public void setRightNavigationListener(@Nullable View.OnClickListener l){
        rightItem.setOnClickListener(l);
    }

    public void navigationLeft(boolean enable){
        leftItem.setVisibility(enable ? View.VISIBLE : View.GONE);
    }

    public void navigationRight(boolean enable){
        rightItem.setVisibility(enable ? View.VISIBLE : View.GONE);
    }

    public User getUser(){
        return user;
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
    }
}
