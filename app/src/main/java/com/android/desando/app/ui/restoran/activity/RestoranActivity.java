package com.android.desando.app.ui.restoran.activity;

import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.desando.app.ui.review_restoran.activity.ReviewRestoranActivity;
import com.android.desando.app.utilities.view.TextViewReadMore;
import com.android.desando.app.R;
import com.android.desando.app.api.DesandoApiService;
import com.android.desando.app.ui.image.browser.activity.ImageBrowserActivity;
import com.android.desando.app.ui.pdf.browser.activity.PdfBrowserActivity;
import com.android.desando.app.ui.restoran.adapter.RestoranExtraAdapter;
import com.android.desando.app.ui.restoran.adapter.RestoranImageAdapter;
import com.android.desando.app.ui.restoran.model.RestoranInfo;
import com.android.desando.app.ui.restoran.model.RestoranTableBean;
import com.android.desando.app.ui.restoran.presenter.RestoranPresenter;
import com.android.desando.app.ui.restoran.view.RestoranView;
import com.android.desando.app.utilities.activity.DesandoActivity;
import com.android.desando.app.utilities.app.ApplicationApp;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.OnClick;
import recycler.coverflow.RecyclerCoverFlow;

import static com.android.desando.app.ui.image.browser.activity.ImageBrowserActivity.ARG_ARRAY_PHOTOS;
import static com.android.desando.app.ui.image.browser.activity.ImageBrowserActivity.ARG_INDEX_PHOTO;
import static com.android.desando.app.ui.pdf.browser.activity.PdfBrowserActivity.ARG_MENU_URL;
import static com.android.desando.app.ui.review_restoran.activity.ReviewRestoranActivity.ARG_TABLE_DATE;
import static com.android.desando.app.ui.review_restoran.activity.ReviewRestoranActivity.ARG_TABLE_ID;

public class RestoranActivity extends DesandoActivity implements RestoranView {
    public static String ARG_RESTORAN_ID = "10001";
    @BindView(R.id.res_desc)
    TextViewReadMore resDesc;
    @BindView(R.id.res_extra)
    RecyclerView resExtra;
    @BindView(R.id.table_online)
    RecyclerView tableOnline;
    @BindView(R.id.res_carusell)
    RecyclerCoverFlow resCarusell;
    @BindView(R.id.res_name)
    TextView resName;
    @BindView(R.id.res_address)
    TextView resAddress;
    @BindView(R.id.res_midle_price)
    TextView resMidlePrice;
    @BindView(R.id.button)
    Button menu;
    @BindView(R.id.res_work_time)
    TextView resWorkTime;
    @BindView(R.id.progress_table_online)
    ProgressBar progressTableOnline;
    @BindView(R.id.res_load)
    ConstraintLayout resLoad;
    @BindView(R.id.res_info)
    NestedScrollView resInfo;

    @BindString(R.string.restoran_des_show)
    String labelShow;
    @BindString(R.string.restoran_des_hide)
    String labelHide;

    RestoranExtraAdapter adapterExtra;
    //RestoranTablesAdapter adapterTable;
    RestoranImageAdapter adapterImage;
    RestoranPresenter presenter;

    @Inject
    DesandoApiService service;
    @BindView(R.id.frameLayout)
    FrameLayout frameLayout;
    @BindView(R.id.btn_order)
    Button btnOrder;

    @Override
    protected void initToolbar() {
        navigationLeft(true);
        setLeftNavigationListener(v -> RestoranActivity.this.finish());
    }

    @Override
    protected int initContentView() {
        return R.layout.activity_restoran;
    }

    @Override
    protected void initView() {
        //resDesc.setTrimCollapsedText(labelShow);
        //resDesc.setTrimExpandedText(labelHide);

        resCarusell.setIntervalRatio((float) 0.25);
        resCarusell.setCameraDistance((float) 0.9);

        adapterImage = new RestoranImageAdapter();
        adapterExtra = new RestoranExtraAdapter();

        adapterExtra.addAllItems(Arrays.asList(new String[]{"Wi-fi", "DJ", "Кальян", "Детская комната", "Туалет"}));
        resExtra.setAdapter(adapterExtra);
        resCarusell.setAdapter(adapterImage);

        tableOnline.setNestedScrollingEnabled(false);

        hideRestoranInfo();
        presenter.getRestoranAbout();

        adapterImage.setOnItemClickListener((parent, view, position, id) -> openImage(position));

        btnOrder.setOnClickListener(v->openOrder(""));
    }

    @Override
    protected void initDaggerInject() {
        ((ApplicationApp)
                this.getApplication())
                .getComponent()
                .inject(this);
        presenter = new RestoranPresenter(service, getIntent().getStringExtra(ARG_RESTORAN_ID));
        presenter.attachView(this);
    }

    @OnClick({R.id.button})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.button:
                openMenu();
                break;
        }
    }

    @Override
    public void showTableList() {
        progressTableOnline.setVisibility(View.GONE);
    }

    @Override
    public void hideTableList() {
        progressTableOnline.setVisibility(View.VISIBLE);
        tableOnline.setVisibility(View.GONE);
    }

    @Override
    public void showRestoranInfo() {
        resLoad.setVisibility(View.GONE);
        resInfo.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideRestoranInfo() {
        resLoad.setVisibility(View.VISIBLE);
        resInfo.setVisibility(View.GONE);
    }

    @Override
    public void setRestoranAbout(RestoranInfo about) {
        showRestoranInfo();

        if (about.getPhotos().size() > 0)
            adapterImage.addAllItems(about.getPhotos());
        else
            frameLayout.setVisibility(View.GONE);

        checkAboutEmpty(about);
        resName.setText(about.getName());
        resAddress.setText(about.getAddress());
        resDesc.setText(about.getDescription());
        resWorkTime.setText(about.getWorktime());

        presenter.memuUrl = about.getMenuLink();

        if (about.getPhotos().size() > 0) {
            resCarusell.getCoverFlowLayout().scrollToPosition(
                    (int) Math.floor(about.getPhotos().size() / 2)
            );
        } else {
            resCarusell.setVisibility(View.GONE);
        }
    }

    @Override
    public void setTableAdapter(List<RestoranTableBean> tables) {
        showTableList();
    }

    @Override
    public void openMenu() {
        Intent intent = new Intent(this, PdfBrowserActivity.class);
        intent.putExtra(ARG_MENU_URL, presenter.memuUrl);
        startActivity(intent);
    }

    @Override
    public void openImage(int pos) {
        Intent intent = new Intent(this, ImageBrowserActivity.class);
        intent.putExtra(ARG_ARRAY_PHOTOS, (Serializable) adapterImage.getAllItem());
        intent.putExtra(ARG_INDEX_PHOTO, pos);
        startActivity(intent);
    }

    @Override
    public void openOrder(String tabId) {
        Intent intent = new Intent(this, ReviewRestoranActivity.class);
        intent.putExtra(ARG_RESTORAN_ID, presenter.restoranId);
        intent.putExtra(ARG_TABLE_ID, tabId);
        intent.putExtra(ARG_TABLE_DATE, presenter.dateTime);
        startActivity(intent);
    }

    @Override
    public void checkAboutEmpty(RestoranInfo about) {
        menuEmpty(about.getMenuLink());
        billEmpty(about.getBill());
    }

    @Override
    public void menuEmpty(String link) {
        if (link == null) {
            menu.setVisibility(View.GONE);
        }

    }

    @Override
    public void tableEmpty(List<RestoranTableBean> tables) {
        if (tables.size() == 0)
            btnOrder.setVisibility(View.VISIBLE);
        else
            btnOrder.setVisibility(View.GONE);

    }

    @Override
    public void galereaEmpty() {

    }

    @Override
    public void extraEmpty() {

    }

    @Override
    public void billEmpty(String bill) {
        if (bill == null)
            resMidlePrice.setVisibility(View.GONE);
        else
            resMidlePrice.setText(getResources().getString(R.string.restoran_lable_middle_price, bill));

    }
}
