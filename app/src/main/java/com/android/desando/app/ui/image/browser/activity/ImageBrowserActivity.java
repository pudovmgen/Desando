package com.android.desando.app.ui.image.browser.activity;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.gw.swipeback.SwipeBackLayout;
import com.android.desando.app.R;
import com.android.desando.app.ui.image.browser.adapter.ImageBrowserAdapter;
import com.android.desando.app.ui.restoran.model.RestoranInfo;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ImageBrowserActivity extends AppCompatActivity implements SwipeBackLayout.OnSwipeBackListener {
    public static String ARG_ARRAY_PHOTOS = "ListPhotos";
    public static String ARG_INDEX_PHOTO = "PhotoIndex";

    @BindView(R.id.image_pager)
    ViewPager imagePager;
    @BindView(R.id.swipeBackLayout)
    SwipeBackLayout swipeBackLayout;

    ImageBrowserAdapter adapter;
    @BindView(R.id.btn_back)
    ImageView btnBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_browser);
        ButterKnife.bind(this);

        adapter = new ImageBrowserAdapter(getSupportFragmentManager(), getData());
        imagePager.setAdapter(adapter);
        imagePager.setCurrentItem(getIntent().getIntExtra(ARG_INDEX_PHOTO,0));
        swipeBackLayout.setSwipeBackListener(this);
        btnBack.setOnClickListener((v)->ImageBrowserActivity.this.finish());
    }

    private List<RestoranInfo.Photo> getData() {
        return (List<RestoranInfo.Photo>) getIntent().getSerializableExtra(ARG_ARRAY_PHOTOS);
    }

    @Override
    public void onViewPositionChanged(View mView, float swipeBackFraction, float swipeBackFactor) {
        swipeBackLayout.invalidate();
        if(btnBack.getVisibility()== View.VISIBLE) btnBack.setVisibility(View.GONE);
    }

    @Override
    public void onViewSwipeFinished(View mView, boolean isEnd) {
        if (isEnd)
            ImageBrowserActivity.this.finish();

            btnBack.setVisibility(View.VISIBLE);
    }
}
