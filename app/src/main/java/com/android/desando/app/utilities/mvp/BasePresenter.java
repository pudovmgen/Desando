package com.android.desando.app.utilities.mvp;

public class BasePresenter <T extends MvpView> implements MvpPresenter<T>{
    private T view;

    public void attachView(T view){
        this.view = view;
    }

    public T getView() {
        return view;
    }

}
