package com.android.desando.app.ui.signin.activity;

import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;

import com.android.desando.app.R;
import com.android.desando.app.api.DesandoApiService;
import com.android.desando.app.ui.signin.presenter.PhonePresenter;
import com.android.desando.app.ui.signin.view.PhoneView;
import com.android.desando.app.utilities.activity.DesandoActivity;
import com.android.desando.app.utilities.app.ApplicationApp;

import javax.inject.Inject;

import br.com.sapereaude.maskedEditText.MaskedEditText;
import butterknife.BindView;
import butterknife.OnClick;

import static com.android.desando.app.ui.signin.activity.PhoneConfirmActivity.ARG_PHONE_CODE;
import static com.android.desando.app.ui.signin.activity.PhoneConfirmActivity.ARG_PHONE_NUMBER;
import static com.android.desando.app.ui.signin.activity.PhoneConfirmActivity.ARG_PHONE_NUMBER_NOT_VAL;

public class SignInActivity extends DesandoActivity implements PhoneView, TextWatcher {
    @Inject
    DesandoApiService service;
    @BindView(R.id.order_phone)
    MaskedEditText orderPhone;
    @BindView(R.id.btn_order)
    Button btnSendPhone;

    PhonePresenter presenter;
    @BindView(R.id.load)
    FrameLayout load;

    @Override
    protected void initToolbar() {
        navigationLeft(true);
        setLeftNavigationListener(v -> SignInActivity.this.finish());
    }

    @Override
    protected int initContentView() {
        return R.layout.activity_order_phone;
    }

    @Override
    protected void initView() {
        orderPhone.setKeepHint(true);
        orderPhone.addTextChangedListener(this);
    }

    @Override
    protected void initDaggerInject() {
        ((ApplicationApp)
                this.getApplication())
                .getComponent()
                .inject(this);
        presenter = new PhonePresenter(service);
        presenter.attachView(this);
    }

    @OnClick(R.id.btn_order)
    public void onViewClicked() {
        sendNumber();
       /* new ConfirmPhoneDialog(this)
                .setOnClickOkLianer(v -> sendNumber())
                .show();*/
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        checkNumber(s + "");
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    @Override
    public void enableSendingNumber() {
        btnSendPhone.setEnabled(true);
    }

    @Override
    public void disableSendingNumber() {
        btnSendPhone.setEnabled(false);
    }

    @Override
    public void checkNumber(String s) {
        if (s.length() < 13) disableSendingNumber();
        else enableSendingNumber();
    }

    @Override
    public void sendNumber() {
        presenter.signIn("7"+orderPhone.getRawText(), "ru");
        showProgress();
        //openConfirm(3000);
    }

    @Override
    public void openConfirm() {
        hideProgress();
        Intent intent = new Intent(this, PhoneConfirmActivity.class);
        intent.putExtra(ARG_PHONE_NUMBER, "+7 " + orderPhone.getText());
        intent.putExtra(ARG_PHONE_NUMBER_NOT_VAL, "7"+orderPhone.getRawText());
        //intent.putExtra(ARG_PHONE_CODE, code);
        startActivityForResult(intent, 2001);
    }

    @Override
    public void showProgress() {
        load.setVisibility(View.VISIBLE);
        btnSendPhone.setVisibility(View.GONE);
    }

    @Override
    public void hideProgress() {
        load.setVisibility(View.GONE);
        btnSendPhone.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode ==2001){
            if(resultCode == RESULT_OK){
                setResult(RESULT_OK);
                SignInActivity.this.finish();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
