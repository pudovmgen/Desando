package com.android.desando.app.ui.signin.activity;

import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.mukesh.OtpView;
import com.android.desando.app.R;
import com.android.desando.app.api.DesandoApiService;
import com.android.desando.app.ui.signin.presenter.PhoneConfirmPresenter;
import com.android.desando.app.ui.signin.view.PhoneConfirmView;
import com.android.desando.app.utilities.activity.DesandoActivity;
import com.android.desando.app.utilities.app.ApplicationApp;

import javax.inject.Inject;

import butterknife.BindView;

public class PhoneConfirmActivity extends DesandoActivity implements PhoneConfirmView {
    public static String ARG_PHONE_NUMBER = "number";
    public static String ARG_PHONE_NUMBER_NOT_VAL = "numberVal";
    public static String ARG_PHONE_CODE = "numberCode";
    @Inject
    DesandoApiService service;
    @BindView(R.id.btn_order)
    Button btnOrder;
    @BindView(R.id.label_phone)
    TextView labelPhone;
    @BindView(R.id.edit_code)
    OtpView editCode;
    @BindView(R.id.load)
    FrameLayout load;

    PhoneConfirmPresenter presenter;

    @Override
    protected void initToolbar() {
        navigationLeft(true);
        setLeftNavigationListener(v -> PhoneConfirmActivity.this.finish());
    }

    @Override
    protected int initContentView() {
        return R.layout.activity_order_confirm_phone;
    }

    @Override
    protected void initView() {
        labelPhone.setText(getIntent().getStringExtra(ARG_PHONE_NUMBER));
        editCode.setListener(this::checkCode);
    }

    @Override
    protected void initDaggerInject() {
        ((ApplicationApp)
                this.getApplication())
                .getComponent()
                .inject(this);

        presenter = new PhoneConfirmPresenter(
                service,
                getIntent().getStringExtra(ARG_PHONE_NUMBER_NOT_VAL),
                getUser()
        );
        presenter.attachView(this);
    }

    @Override
    public void showProgress() {
        load.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        load.setVisibility(View.GONE);
    }

    @Override
    public void checkCode(String c) {
       // if(presenter.code.equals(c)){
            showProgress();
            presenter.confirmPhone(c);
        //}
    }

    @Override
    public void successfulAuth(String token) {
        getUser().setPhone(getIntent().getStringExtra(ARG_PHONE_NUMBER));
        getUser().setToken(token);
        presenter.updatePhone(token);
    }

    @Override
    public void successfulUpdate() {
        hideProgress();
        setResult(RESULT_OK);
        PhoneConfirmActivity.this.finish();
    }


}
