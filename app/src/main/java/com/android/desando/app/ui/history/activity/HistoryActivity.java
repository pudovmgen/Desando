package com.android.desando.app.ui.history.activity;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.desando.app.R;
import com.android.desando.app.api.DesandoApiService;
import com.android.desando.app.ui.history.adapter.HistoryAdapter;
import com.android.desando.app.ui.history.model.HistoryBean;
import com.android.desando.app.ui.history.presenter.HistoryPresenter;
import com.android.desando.app.ui.history.view.HistoryView;
import com.android.desando.app.utilities.activity.DesandoActivity;
import com.android.desando.app.utilities.app.ApplicationApp;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

import static com.android.desando.app.ui.history.activity.OpenOrderHistoryActivity.ARG_ORDER_ID;

public class HistoryActivity extends DesandoActivity implements HistoryView {
    @BindView(R.id.res_history)
    RecyclerView resHistory;
    @BindView(R.id.progress)
    ProgressBar progress;
    @BindView(R.id.empty_text)
    TextView emptyText;
    @Inject
    DesandoApiService service;

    HistoryPresenter presenter;
    HistoryAdapter adapter;

    @Override
    protected void initToolbar() {
        navigationLeft(true);
        setLeftNavigationListener(v -> HistoryActivity.this.finish());
    }

    @Override
    protected int initContentView() {
        return R.layout.activity_history;
    }

    @Override
    protected void initView() {
        adapter = new HistoryAdapter();
        resHistory.setAdapter(adapter);
        adapter.setOnItemSelected((item, pos) -> openOrder(item));
        hideList();
        presenter.getHistory();
    }

    @Override
    protected void initDaggerInject() {
        ((ApplicationApp)
                this.getApplication())
                .getComponent()
                .inject(this);

        presenter = new HistoryPresenter(
                service,
                getUser()
        );
        presenter.attachView(this);

    }

    @Override
    public void showList() {
        progress.setVisibility(View.GONE);
        resHistory.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideList() {
        progress.setVisibility(View.VISIBLE);
        resHistory.setVisibility(View.GONE);
    }

    @Override
    public void showEmpty() {
        progress.setVisibility(View.GONE);
        emptyText.setVisibility(View.VISIBLE);
    }

    @Override
    public void setList(List<HistoryBean> history) {
        showList();
        adapter.addAllItems(history);
    }

    @Override
    public void openOrder(HistoryBean order) {

        Intent intent= new Intent(this, OpenOrderHistoryActivity.class);
        intent.putExtra(ARG_ORDER_ID, order.getId());
        startActivity(intent);
    }
}
