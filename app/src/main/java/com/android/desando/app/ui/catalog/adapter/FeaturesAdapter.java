package com.android.desando.app.ui.catalog.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.android.desando.app.R;
import com.android.desando.app.ui.catalog.model.FeaturesBean;
import com.android.desando.app.utilities.adapter.BaseAdapterRecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FeaturesAdapter extends BaseAdapterRecyclerView<FeaturesBean, FeaturesAdapter.FeaturesHolder> {
    @Override
    protected FeaturesHolder onCreateHolder(ViewGroup viewGroup, int viewType) {
        return new FeaturesHolder(LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_tag, viewGroup, false));
    }

    @Override
    protected void onBindHolder(FeaturesHolder holder, int position) {
        FeaturesBean model = getItem(position);
        holder.tag.setText(model.getNameRu());

        holder.tag.setOnClickListener(v-> callbackOnSelected.onItemSelected(model, position));
    }


    class FeaturesHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tag)
        Button tag;

        public FeaturesHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}
