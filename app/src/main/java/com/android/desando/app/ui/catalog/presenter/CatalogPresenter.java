package com.android.desando.app.ui.catalog.presenter;

import android.annotation.SuppressLint;
import android.util.Log;

import com.android.desando.app.api.DesandoApiService;
import com.android.desando.app.ui.catalog.model.FeaturesBean;
import com.android.desando.app.ui.catalog.view.CatalogView;
import com.android.desando.app.utilities.mvp.BasePresenter;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class CatalogPresenter extends BasePresenter<CatalogView>  {
    DesandoApiService service;
    private String cityId;

    public CatalogPresenter(DesandoApiService service,String cityId) {
        this.service = service;
        this.cityId = cityId;
    }


    @SuppressLint("CheckResult")
    public void getRestorans(){
        Log.d("cityid",cityId);
        service.getRestorans(cityId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                        getView()::setRestoransList,
                        throwable ->{}
                );
    }

    @SuppressLint("CheckResult")
    public void getFeaturesRestorans(String features){
        service.getFeaturesRestorans(features,cityId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                        getView()::setRestoransList,
                        throwable ->{}
                );
    }

    @SuppressLint("CheckResult")
    public void getFeatures(){

        service.getFeatures(cityId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                        restoranBeans -> {
                            setFeaturesMain(restoranBeans);
                        },
                        throwable ->{}
                );
    }

    @SuppressLint("CheckResult")
    public void setFeaturesMain(List<FeaturesBean> features){
        Observable.fromArray(features)
                .flatMapIterable(list -> list)
                .filter(featuresBean -> featuresBean.getStatus().equals("master"))
                .subscribe(
                      v -> {
                          getView().setFeatures(v);
                          //getView().setTagList(features);
                      },throwable -> {},
                        ()->{
                            for(int i=0;i<features.size();i++){
                                if (features.get(i).getStatus().equals("master")){
                                    features.remove(i);
                                }
                            }
                            getView().setTagList(features);
                        }
                );
    }

    @SuppressLint("CheckResult")
    public void getSearch(String s){
        service.getSearch(cityId,s,0,200)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                        getView()::setRestoransList,
                        throwable ->{}
                );
    }

}
