package com.android.desando.app.utilities.app;

import android.app.Application;

import com.gw.swipeback.tools.WxSwipeBackActivityManager;
import com.android.desando.app.utilities.di.AppComponent;
import com.android.desando.app.utilities.di.DaggerAppComponent;
import com.android.desando.app.utilities.di.NetModule;

public class ApplicationApp extends Application {
    private static AppComponent component;
    @Override
    public void onCreate() {
        super.onCreate();
        initComponent();
    }

    private void initComponent() {
        component =  DaggerAppComponent.builder()
                .netModule(new NetModule("https://my.desando.ru/"))
                .build();
    }

    public static AppComponent getComponent(){
        return component;
    }
}
