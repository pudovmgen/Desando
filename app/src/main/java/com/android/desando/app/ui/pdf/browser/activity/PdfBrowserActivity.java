package com.android.desando.app.ui.pdf.browser.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import com.google.common.io.Files;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.github.barteksc.pdfviewer.PDFView;
import com.android.desando.app.R;
import com.android.desando.app.api.DesandoApiService;
import com.android.desando.app.utilities.app.ApplicationApp;

import java.io.File;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import okio.BufferedSink;
import okio.Okio;
import retrofit2.Response;

public class PdfBrowserActivity extends AppCompatActivity {
    public static String ARG_MENU_URL = "MenuUrl";
    @BindView(R.id.pdfView)
    PDFView pdfView;

    @Inject
    DesandoApiService service;
    @BindView(R.id.progress)
    ProgressBar progress;

    @SuppressLint("CheckResult")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf_browser);
        ButterKnife.bind(this);

        ((ApplicationApp)
                this.getApplication())
                .getComponent()
                .inject(this);

        progress.setVisibility(View.VISIBLE);
        pdfView.setVisibility(View.GONE);
        service.downloadPdf(getIntent().getStringExtra(ARG_MENU_URL))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response->{
                    Log.i("PDFFFFF",response.string().length()+"" );
                    /*pdfView.fromBytes(response.string().getBytes())
                            .defaultPage(1)
                            .swipeHorizontal(true)
                            .load();
                    progress.setVisibility(View.GONE);
                    pdfView.setVisibility(View.VISIBLE);*/
                });
 /*       service.downloadPdf(getIntent().getStringExtra(ARG_MENU_URL))
                .flatMap((Function<Response<ResponseBody>, ObservableSource<?>>) responseBodyResponse -> saveToDiskRx(responseBodyResponse))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(o -> {

                    progress.setVisibility(View.GONE);
                    pdfView.setVisibility(View.VISIBLE);

                    pdfView.fromFile(destinationFile1)
                            .defaultPage(1)
                            .swipeHorizontal(true)
                            .load();
                });*/







/*        progress.setVisibility(View.GONE);
        pdfView.setVisibility(View.VISIBLE);

        pdfView.fromFile(file)
                .defaultPage(1)
                .swipeHorizontal(true)
                .load();*/


    }

    public static final String md5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    private Observable<File> saveToDiskRx(final Response<ResponseBody> response) {
        return Observable.create(new ObservableOnSubscribe<File>() {
            @Override
            public void subscribe(ObservableEmitter<File> emitter) throws Exception {
                ///
                String header = response.headers().get("Content-Disposition");
                String filename = header.replace("attachment; filename=", "");

                new File("/data/data/" + getPackageName() + "/games").mkdir();
                File destinationFile = new File("/data/data/" + getPackageName() + "/games/" + filename);
                destinationFile1 = destinationFile;
                BufferedSink bufferedSink = Okio.buffer(Okio.sink(destinationFile));
                bufferedSink.writeAll(response.body().source());
                bufferedSink.close();

                emitter.onNext(destinationFile);
                emitter.onComplete();
            }
        });
    }
    File destinationFile1;
}
