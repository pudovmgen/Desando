package com.android.desando.app.ui.catalog.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.android.desando.app.R;
import com.android.desando.app.ui.catalog.model.RestoranBean;
import com.android.desando.app.utilities.adapter.BaseAdapterRecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RestoranAdapter extends BaseAdapterRecyclerView<RestoranBean, RestoranAdapter.RestoranHolder> {
    @Override
    protected RestoranAdapter.RestoranHolder onCreateHolder(ViewGroup viewGroup, int viewType) {
        return new RestoranAdapter.RestoranHolder(LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_restaran, viewGroup, false));
    }

    @Override
    protected void onBindHolder(RestoranHolder holder, int position) {
        RestoranBean model =getItem(position);
        holder.resName.setText(model.getName());
        holder.resAddress.setText(model.getAddress());
        /*holder.resClear.setText(
                holder.
                itemView.getResources()
                        .getQuantityString(R.plurals.free_tables_format,model.getCount(),model.getCount())
        );*/

        Glide.with(holder.itemView.getContext())
                .load(model.getMasterPhoto())
                .error(R.drawable.image_not_available)
                .into(holder.resImage);

        holder.itemView.setOnClickListener(v -> callbackOnSelected.onItemSelected(model, position));

    }



    class RestoranHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.res_image)
        ImageView resImage;
        @BindView(R.id.res_name)
        TextView resName;
        @BindView(R.id.res_address)
        TextView resAddress;
        @BindView(R.id.res_clear)
        TextView resClear;


        public RestoranHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }




}
