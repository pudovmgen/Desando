package com.android.desando.app.ui.signin.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CheckPhoneBean implements Serializable
{

    @SerializedName("status")
    @Expose
    private Boolean status;
    private final static long serialVersionUID = 7629808471044353627L;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

}