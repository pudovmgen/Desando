package com.android.desando.app.utilities.view;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.TextView;

import com.android.desando.app.R;

public class TextViewReadMore extends ConstraintLayout {
    TextView text;
    TextView readMore;
    public TextViewReadMore(Context context) {
        super(context);
        init();
    }

    public TextViewReadMore(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TextViewReadMore(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }


    private void init() {
        inflate(getContext(), R.layout.view_read_more, this);
        this.text = (TextView)findViewById(R.id.text);
        this.readMore = (TextView)findViewById(R.id.read_more);

        this.text.setOnClickListener(v->toogleText());
        this.readMore.setOnClickListener(v->toogleText());
        ///
        //this.text.setMaxLines(4);
        this.readMore.setText(getResources().getString(R.string.restoran_des_show));
    }

    private void toogleText(){
        if (text.getMaxLines() == 4){
            text.setMaxLines(10000);
            readMore.setText(getResources().getString(R.string.restoran_des_hide));
        }else {
            text.setMaxLines(4);
            readMore.setText(getResources().getString(R.string.restoran_des_show));
        }
    }

    public void setText(String s){
        text.setMaxLines(10000);
        this.text.setText(s);

        this.text.post(() -> {
            if (this.text.getLineCount() <= 4){
                readMore.setVisibility(GONE);
            }else {
                text.setMaxLines(4);
                readMore.setVisibility(VISIBLE);
            }
        });
    }
}
