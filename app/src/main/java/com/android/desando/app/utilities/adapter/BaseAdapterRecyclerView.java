package com.android.desando.app.utilities.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class BaseAdapterRecyclerView<T, E extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<E>  {
    ArrayList<T> items;
    protected OnItemSelectedListener callbackOnSelected;

    public BaseAdapterRecyclerView(ArrayList<T> items) {
        this.items = items;
        init();
    }

    public BaseAdapterRecyclerView(List<T> items) {
        this.items = (ArrayList<T>) items;
        init();
    }

    public BaseAdapterRecyclerView() {
        this.items = new ArrayList<>();
        init();
    }

    private void init(){
    }

    public void addItems(T item){
        this.items.add(item);
        notifyDataSetChanged();

    }

    public void addItems(int start, T item){
        this.items.add(start,item);
        notifyDataSetChanged();
    }

    public void addAllItems(ArrayList<T> items){
        this.items.addAll(items);
        notifyDataSetChanged();
    }

    public void addAllItems(String[] items){
        this.items.addAll(new ArrayList(Arrays.asList(items)));
        notifyDataSetChanged();
    }

    public void addAllItems(List<T> items){
        this.items.addAll(items);
        notifyDataSetChanged();
    }

    public void addAllItemsInClear(List<T> items){
        this.items.clear();
        this.items.addAll(items);
        notifyDataSetChanged();
    }

    public void addAllItems(int start,ArrayList<T> items){
        this.items.addAll(start, items);
        notifyDataSetChanged();
    }
    public void addAllItems(int start,List<T> items){
        this.items.addAll(start, items);
        notifyDataSetChanged();
    }

    public void removeItem(int index){
        this.items.remove(index);
        notifyDataSetChanged();
    }

    public T getItem(int index){
        return items.get(index);
    }

    public List<T> getAllItem(){
        return items;
    }


    protected abstract E onCreateHolder(ViewGroup viewGroup, int viewType);

    protected abstract void onBindHolder(E holder, int position);

    @Override
    public E onCreateViewHolder(ViewGroup parent, int viewType) {
        return onCreateHolder(parent, viewType);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        this.onBindHolder((E)holder, position);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    public interface OnItemSelectedListener<T>{
        void onItemSelected(T item, int pos);
    }
    public void setOnItemSelected(OnItemSelectedListener<T> onItemSelected){
        this.callbackOnSelected = onItemSelected;
    }

}