package com.android.desando.app.ui.signin.presenter;

import android.annotation.SuppressLint;
import android.util.Log;

import com.android.desando.app.api.DesandoApiService;
import com.android.desando.app.ui.signin.model.PhoneConfirmBean;
import com.android.desando.app.ui.signin.model.SignInBean;
import com.android.desando.app.ui.signin.view.PhoneView;
import com.android.desando.app.utilities.mvp.BasePresenter;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class PhonePresenter extends BasePresenter<PhoneView>  {
    DesandoApiService service;

    public PhonePresenter(DesandoApiService service) {
        this.service = service;
    }

    @SuppressLint("CheckResult")
    public void signIn(String phone, String country){
        service.signIn(new SignInBean(phone,country))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                        success->{
                         getView().openConfirm();
                        },
                        throwable ->{
                            Log.d("API", throwable.getMessage());
                        }
                );
    }

}
