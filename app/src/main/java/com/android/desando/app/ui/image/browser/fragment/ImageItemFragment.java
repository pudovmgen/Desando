package com.android.desando.app.ui.image.browser.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.android.desando.app.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ImageItemFragment extends Fragment {
    public static String ARG_IMAGE= "Image";

    @BindView(R.id.image_item)
    ImageView imageItem;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(
                R.layout.fragment_browser_image, container, false);
        ButterKnife.bind(this, rootView);

        Glide.with(this)
                .load(getArguments().getString(ARG_IMAGE))
                .into(imageItem);

        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
