package com.android.desando.app.ui.catalog.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.android.desando.app.R;
import com.android.desando.app.api.DesandoApiService;
import com.android.desando.app.ui.catalog.adapter.FeaturesAdapter;
import com.android.desando.app.ui.catalog.adapter.RestoranAdapter;
import com.android.desando.app.ui.catalog.model.FeaturesBean;
import com.android.desando.app.ui.catalog.model.RestoranBean;
import com.android.desando.app.ui.catalog.presenter.CatalogPresenter;
import com.android.desando.app.ui.catalog.view.CatalogPlanView;
import com.android.desando.app.ui.catalog.view.CatalogView;
import com.android.desando.app.ui.history.activity.HistoryActivity;
import com.android.desando.app.ui.restoran.activity.RestoranActivity;
import com.android.desando.app.ui.signin.activity.SignInActivity;
import com.android.desando.app.utilities.activity.DesandoActivity;
import com.android.desando.app.utilities.app.ApplicationApp;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

import static com.android.desando.app.ui.restoran.activity.RestoranActivity.ARG_RESTORAN_ID;

public class CatalogActivity extends DesandoActivity implements CatalogView, CatalogPlanView {
    public static String ARG_CITY_ID = "cityId";
    //view
    @BindView(R.id.list_restaurant)
    RecyclerView listRes;
    @BindView(R.id.list_tag)
    RecyclerView listTag;
    @BindView(R.id.progress)
    ProgressBar progress;
    @BindView(R.id.nav_view)
    NavigationView navView;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.features_image)
    ImageView featuresImage;
    @BindView(R.id.features_text)
    TextView featuresText;
    @BindView(R.id.features_count)
    TextView featuresCount;
    @BindView(R.id.nestedScrollView)
    NestedScrollView nestedScrollView;
    //adapter
    RestoranAdapter adapterRes;
    FeaturesAdapter adapterFeat;
    //
    @Inject
    DesandoApiService service;
    CatalogPresenter presenter;
    @BindView(R.id.header)
    ConstraintLayout header;
    @BindView(R.id.search)
    EditText search;
    @BindView(R.id.search_close)
    ImageView searchClose;

    //@SuppressLint("CheckResult")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initNavigationView();
    }

    private void initNavigationView() {
        View headerview = navView.getHeaderView(0);
        headerview.findViewById(R.id.nav_menu_back).setOnClickListener(v -> drawerLayout.closeDrawer(GravityCompat.START));
        ((TextView) headerview.findViewById(R.id.user_name)).setText(getUser().getName());
        ((TextView) headerview.findViewById(R.id.user_phone)).setText(getUser().getPhone());
        ((TextView) headerview.findViewById(R.id.user_city)).setText(getUser().getCityName());

        headerview.findViewById(R.id.user_history).setOnClickListener(v -> {
            if (getUser().isAuth()) {
                drawerLayout.closeDrawer(GravityCompat.START);
                startActivity(new Intent(CatalogActivity.this, HistoryActivity.class));
            } else {
                openDialogAuth();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        initNavigationView();
    }

    @Override
    protected int initContentView() {
        return R.layout.activity_catalog;
    }

    @Override
    protected void initView() {
        listRes.setNestedScrollingEnabled(false);
        //
        adapterRes = new RestoranAdapter();
        adapterFeat = new FeaturesAdapter();
        listRes.setAdapter(adapterRes);
        listTag.setAdapter(adapterFeat);


        adapterRes.setOnItemSelected((item, pos) -> openRestoran(item.getRestoranId()));
        adapterFeat.setOnItemSelected((item, pos) -> selectionFeatures(item));
        searchClose.setOnClickListener(v -> primaryEntrance());
        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                search(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
                // do stuff
            }
        });

        primaryEntrance();
    }

    @Override
    protected void initToolbar() {
        navigationLeft(true);
        navigationRight(true);
        setRightNavigationDrawable(R.drawable.ic_toolbar_search);
        setRightNavigationListener(v -> showSearch());
    }

    @Override
    protected void initDaggerInject() {
        ((ApplicationApp)
                this.getApplication())
                .getComponent()
                .inject(this);

        presenter = new CatalogPresenter(
                service,
                getIntent().getStringExtra(ARG_CITY_ID)
        );
        presenter.attachView(this);
    }

    @Override
    public void hideRestoransList() {
        progress.setVisibility(View.VISIBLE);
        listRes.setVisibility(View.GONE);
    }

    @Override
    public void showRestoransList() {
        progress.setVisibility(View.GONE);
        listRes.setVisibility(View.VISIBLE);
    }

    @Override
    public void setRestoransList(List<RestoranBean> items) {
        adapterRes.addAllItemsInClear(items);
        showRestoransList();
    }


    @Override
    public void setTagList(List<FeaturesBean> tags) {
        adapterFeat.addAllItemsInClear(tags);
    }

    @Override
    public void setFeatures(FeaturesBean features) {
        Glide.with(this)
                .load(features.getPhoto())
                .into(featuresImage);
        featuresCount.setText(features.getCount().toString());
        featuresText.setText(features.getNameRu());
        featuresImage.setOnClickListener(v->selectionFeatures(features));
        showFeaturesImage();
    }

    @Override
    public void hideSearch() {
                        header.setVisibility(View.VISIBLE);
                        search.setVisibility(View.GONE);
                        searchClose.setVisibility(View.GONE);
    }

    @Override
    public void showSearch() {
        search.setVisibility(View.VISIBLE);
        searchClose.setVisibility(View.VISIBLE);
        header.setVisibility(View.GONE);
    }

    @Override
    public void hideFeaturesImage() {
        header.setVisibility(View.GONE);
        listTag.setVisibility(View.GONE);
    }

    @Override
    public void showFeaturesImage() {
        header.setVisibility(View.VISIBLE);
        listTag.setVisibility(View.VISIBLE);
    }

    @Override
    public void openRestoran(String restoranId) {
        Intent intent = new Intent(this, RestoranActivity.class);
        intent.putExtra(ARG_RESTORAN_ID, restoranId);
        startActivity(intent);
    }

    @Override
    public void openFeatures(FeaturesBean features) {
        presenter.getFeaturesRestorans(features.getNameEn());
    }

    @Override
    public void openDialogAuth() {
        new AlertDialog.Builder(this)
                .setTitle(R.string.res_dialog_title)
                .setMessage(R.string.res_dialog_message)
                .setPositiveButton(R.string.res_dialog_auth, (dialog, id) -> openAuth())
                .setNegativeButton(R.string.res_dialog_close, (dialog, id) -> {
                })
                .create()
                .show();
    }

    @Override
    public void openAuth() {
        startActivityForResult(new Intent(this, SignInActivity.class), 200);
    }

    @Override
    public void search(String s) {
        hideRestoransList();
        presenter.getSearch(s);
    }

    @Override
    public void primaryEntrance() {
        hideRestoransList();
        hideFeaturesImage();
        hideSearch();
        presenter.getRestorans();
        presenter.getFeatures();
        setLeftNavigationDrawable(R.drawable.ic_toolbar_menu);
        setLeftNavigationListener(v -> drawerLayout.openDrawer(GravityCompat.START));
    }

    @Override
    public void selectionFeatures(FeaturesBean features) {
        hideSearch();
        hideFeaturesImage();
        hideRestoransList();
        setLeftNavigationDrawable(R.drawable.ic_toolbar_arrow);
        setLeftNavigationListener(v -> primaryEntrance());
        openFeatures(features);
    }

    @Override
    public void selectionSearch() {
        setLeftNavigationDrawable(R.drawable.ic_toolbar_arrow);
        setLeftNavigationListener(v -> primaryEntrance());
        showSearch();
    }
}
