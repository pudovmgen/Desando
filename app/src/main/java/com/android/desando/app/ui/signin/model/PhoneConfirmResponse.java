package com.android.desando.app.ui.signin.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PhoneConfirmResponse implements Serializable
{

    @SerializedName("token")
    @Expose
    private String token;
    private final static long serialVersionUID = -7590683264531534519L;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}