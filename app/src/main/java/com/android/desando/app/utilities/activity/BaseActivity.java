package com.android.desando.app.utilities.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.android.desando.app.R;

import butterknife.ButterKnife;

public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(initContentView());
        ButterKnife.bind(this);
        initDaggerInject();
        initView();
    }

    protected abstract int initContentView();

    protected abstract void initView();

    protected abstract void initDaggerInject();
}
