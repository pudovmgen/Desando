package com.android.desando.app.ui.restoran.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class RestoranInfo implements Serializable
{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("worktime")
    @Expose
    private String worktime;
    @SerializedName("bill")
    @Expose
    private String bill;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("menu_link")
    @Expose
    private String menuLink;
    @SerializedName("additions")
    @Expose
    private List<Object> additions = null;
    @SerializedName("tables")
    @Expose
    private List<RestoranTableBean> tables = null;
    @SerializedName("photos")
    @Expose
    private List<Photo> photos = null;
    private final static long serialVersionUID = 7310971036220381502L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getWorktime() {
        return worktime;
    }

    public void setWorktime(String worktime) {
        this.worktime = worktime;
    }

    public String getBill() {
        return bill;
    }

    public void setBill(String bill) {
        this.bill = bill;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMenuLink() {
        return menuLink;
    }

    public void setMenuLink(String menuLink) {
        this.menuLink = menuLink;
    }

    public List<Object> getAdditions() {
        return additions;
    }

    public void setAdditions(List<Object> additions) {
        this.additions = additions;
    }

    public List<RestoranTableBean> getTables() {
        return tables;
    }

    public void setTables(List<RestoranTableBean> tables) {
        this.tables = tables;
    }

    public List<Photo> getPhotos() {
        return photos;
    }

    public void setPhotos(List<Photo> photos) {
        this.photos = photos;
    }

    public static class Photo implements Serializable
    {

        @SerializedName("photo_id")
        @Expose
        private Integer photoId;
        @SerializedName("link")
        @Expose
        private String link;

        @SerializedName("link_mini")
        @Expose
        private String linkSmail;

        private final static long serialVersionUID = 799297530554216068L;

        public Photo(String link) {
            this.link = link;
        }

        public Integer getPhotoId() {
            return photoId;
        }

        public void setPhotoId(Integer photoId) {
            this.photoId = photoId;
        }

        public String getLink() {
            return link;
        }

        public void setLink(String link) {
            this.link = link;
        }

        public String getLinkSmail() {
            return linkSmail;
        }

        public void setLinkSmail(String link_smail) {
            this.linkSmail = link_smail;
        }
    }

}
