package com.android.desando.app.ui.review_restoran.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.desando.app.R;
import com.android.desando.app.ui.review_restoran.adapter.OrderTimesAdapter;

import java.io.Serializable;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class DateFragment extends Fragment implements Serializable {
    public static String ARG_TIMES_LIST = "ARG_TIMES_LIST";
    public static String ARG_CALLBACK = "ARG_CALLBACK";
    @BindView(R.id.list_date)
    RecyclerView listDate;
    Unbinder unbinder;
    OrderTimesAdapter adapter;
    OrderTimesAdapter.OrderTimesCallback timesCallback;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_date_pager, container, false);
        unbinder = ButterKnife.bind(this, view);
        initView();
        return view;
    }

    private void initView() {
        adapter = new OrderTimesAdapter();
        timesCallback =(OrderTimesAdapter.OrderTimesCallback) getArguments().getSerializable(ARG_CALLBACK);
        adapter.setOnItemClickListener(timesCallback);
        listDate.setAdapter(adapter);
        adapter.addAllItems(getArguments().getStringArray(ARG_TIMES_LIST));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
